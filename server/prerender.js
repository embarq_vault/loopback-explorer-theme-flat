const puppeteer = require('puppeteer');

module.exports.prerender = async function(url) {
  const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
  const page = await browser.newPage();
  await page.goto(url, {waitUntil: 'networkidle2'});
  const html = await page.content();

  await browser.close();
  return html;
}
