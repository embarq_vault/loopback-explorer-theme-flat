'use strict';

const fs = require('fs');
const path = require('path');
const loopback = require('loopback');
const boot = require('loopback-boot');

const {prerender} = require('./prerender');
const app = module.exports = loopback();
const PRERENDER_MIDDLEWARE_CACHE_SRC = 'prerender-service-src';
const PRERENDER_MIDDLEWARE_CACHE_READY = 'prerender-middleware-cache-ready';

app.set(PRERENDER_MIDDLEWARE_CACHE_READY, false);

app.use((req, res, next) => {
  if (!app.get(PRERENDER_MIDDLEWARE_CACHE_READY)) {
    return next();
  }

  if (!/^\/explorer\/?$/.test(req.path)) {
    return next();
  }

  const cachedDocs = app.get(PRERENDER_MIDDLEWARE_CACHE_SRC);

  if (cachedDocs) {
    console.log(`[GET ${ req.path }}] docs cache found`);
    res.send(cachedDocs);
  } else {
    console.log(`[GET ${ req.path }}] docs cache is missing`)
    next();
  }
});

app.start = function() {

  // start the web server
  return app.listen(function() {
    const baseUrl = app.get('url').replace(/\/$/, '');

    console.log('Web server listening at: %s', baseUrl);

    if (app.get('loopback-component-explorer')) {
      const explorerPath = app.get('loopback-component-explorer').mountPath;

      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);

      prerender(path.join(baseUrl, explorerPath)).then(prerenderedDocs => {
        app.set(PRERENDER_MIDDLEWARE_CACHE_READY, true);
        app.set(PRERENDER_MIDDLEWARE_CACHE_SRC, prerenderedDocs);
        console.log('[Docs prerender] Prerendered docs successfully cached');
      }).catch(err => {
        console.warn('[Docs prerender]');
        console.error(err);
      })
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
